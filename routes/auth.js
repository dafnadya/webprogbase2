const express = require('express');
const User = require("../models/user");
const extra = require('./extra');
const passport = require('passport');
const LocalStrategy = require('passport-local').Strategy;
const GoogleStrategy = require('passport-google-oauth20');
const config = require("../config");

const router = express.Router();

passport.use(new LocalStrategy(
    function (username, password, done) {
    let hash = extra.sha512(password).passwordHash;
    User.getByUsername(username)
    .then(user => {
		if (user && user.passwordHash === hash) 
            return done(null, user);
        else
            return done(null, false);
    });
}));

passport.use(new GoogleStrategy(
	{
		callbackURL: "/auth/google/redirect",
		clientID: config.clientID,
		clientSecret: config.clientSecret
	}, (accessToken, refreshToken, profile, done) => {
		User.getByGoogleId(profile.id)
		.then(user => {
			if (user) {
				return done(null, user);
			}
			else {
				let user = new User("g" + profile.id, null, profile.displayName);
				user.googleId = profile.id;
				user.email = profile.emails[0].value;
				user.avaUrl = profile.photos[0].value;
				User.insert(user)
				.then(user => {
					return done(null, user);
				})
				.catch(err => done(err));
			}
		})
		.catch(err => { 
			return done(err);
		});
	})
)

passport.serializeUser(function(user, done) {
    done(null, user._id);
});

passport.deserializeUser(function(id, done) {
    User.getById(id)
    .then(user => {
        done(user ? null : 'No user', user);
    });
});

router.post('/login',
    passport.authenticate('local', { 
		successRedirect: '/',
        failureRedirect: '/auth/login?error=Incorrect+username+or+password' 
    })
);

router.get(`/register`, function (req, res) {
	let error = req.query.error;
    res.render(`register`, {error});
});

router.post(`/register`, function (req, res) {
	const username = req.body.username;
    User.getByUsername(username)
	.then(user => {
		if (!user) {
			if (req.body.password === req.body.password2) {
				const fullname = req.body.fullname;
				const password = req.body.password;
				User.insert(new User(username, password, fullname))
				.then(() => res.redirect(`/auth/login`))
				.catch(err => res.render(`error`, {error403: 500, error: err}));
				return;
			}
			else {
				res.redirect('/auth/register?error=Passwords+do+not+match');
				return;
			}
		}
		else {
			res.redirect('/auth/register?error=Username+already+exists');
			return;
		}
	});
});

router.get(`/login`, function (req, res) {
	let error = req.query.error;
    res.render(`login`, {error: error});
});

router.get(`/google`, passport.authenticate('google', {
	scope: ['profile', 'email']
}));

router.get(`/google/redirect`, passport.authenticate('google'), (req, res) => {
    res.redirect('/');
});

router.get(`/logout`, extra.checkAuth, function (req, res) {
    req.logout();   
    res.redirect('/');
});

module.exports = router;