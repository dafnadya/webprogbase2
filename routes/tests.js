const express = require('express');
const Test = require("../models/test");
const PassedTest = require("../models/passedTest");
const Question = require("../models/question");
const User = require("../models/user");
const extra = require("./extra");
const Telegram = require("../telegram");

const router = express.Router();

router.get(`/`, extra.checkAuth, function (req, res) {
    res.render(`tests`, {user: req.user});
});

router.get(`/testsWall`, function (req, res) {
    res.render(`testsWall`, {user: req.user});
});

router.get(`/pass/:id`, function (req, res) {
    Test.getById(req.params.id)
    .then(test => res.render(`passTest`, {user: req.user, test: test}))
    .catch(err => res.render(`error`, {error500: 500, error: err}));
});

router.post(`/addQuestions`, extra.checkAuth, function (req, res) {
    const testId = req.body.testId;
    let selectedQuestionsId = req.body.quest_select;
    if (typeof selectedQuestionsId === `string`) {
        selectedQuestionsId = [req.body.quest_select];
    }
    Test.addQuestions(testId, selectedQuestionsId)
    .then(() => {
        for (let questionId of selectedQuestionsId) {
            Question.addedToTest(questionId, testId);
        }
    })
    .then(() => res.redirect(`/tests/${testId}`))
    .catch(err => res.render(`error`, {error500: 500, error: err}));
});

router.post(`/addResult`, function (req, res) {
    let testId = req.body.testId;
    let rightAnswers = req.body.rightAnswers;
    let passedTest = new PassedTest(testId, rightAnswers);
    if (req.user) {
        PassedTest.insert(passedTest)
        .then(passedTest => User.addResult(req.user.id, passedTest.id))
        .catch(err => res.render(`error`, {error500: 500, error: err}));
    }
    let questionsAmount = req.body.questionsAmount;
    let percent = parseInt(rightAnswers * 100 / questionsAmount)
    let text = "";
    if (percent > 50) {
        text = "Congratulation! Great result.";
    }
    else {
        text = "You should try harder!";
    }
    Test.getById(testId)
    .then(test => {
        try {
            if (req.user && req.user.telegramUsername) {
                let text = `You've passed test: [${test.title}](http://dafnadya-lab6.herokuapp.com/tests/pass/${test.id}).\n`
                + `Result: *${rightAnswers}*/${questionsAmount}\n`
                + `More tests on [test wall](http://dafnadya-lab6.herokuapp.com/tests/testsWall).`;
                Telegram.sendNotificationToUser(text, req.user.telegramUsername);
            }
            res.render(`result`, {user: req.user, rightAnswers: rightAnswers, questionsAmount: questionsAmount,
            percent: percent, text: text, test: test });
            } catch (err) {
                res.render(`error`, {error500: 500, error: err});
            }
    })
    .catch(err => res.render(`error`, {error500: 500, error: err}));
});

router.post(`/activate`, extra.checkAuth, function (req, res) {
    const testId = req.body.testId;
    Test.getById(testId)
    .then(test => {
        test.isActive = true;
        return Test.update(test);
    })
    .then(test => res.redirect(`/tests/${test.id}`))
    .catch(err => res.render(`error`, {error500: 500, error: err}));
});

router.post(`/inactivate`, extra.checkAuth, function (req, res) {
    const testId = req.body.testId;
    Test.getById(testId)
    .then(test => {
        test.isActive = false;
        return Test.update(test);
    })
    .then(test => res.redirect(`/tests/${test.id}`))
    .catch(err => res.render(`error`, {error500: 500, error: err}));
});

router.post(`/removeQuestion`, extra.checkAuth, function (req, res) {
    const testId = req.body.testId;
    let questId = req.body.questionId;
    Test.removeQuestion(testId, questId)
    .then(()=> {
        Question.removedFromTest(questId, testId);
    })
    .then(() => res.redirect(`/tests/${testId}`))
    .catch(err => res.render(`error`, {error500: 500, error: err}));
});

router.get(`/update/:id`, extra.checkAuth, function (req, res) {
    Test.getById(req.params.id)
    .then(test => {
        res.render(`tests/update`, {test: test, user: req.user, action: "/tests/update/" + req.params.id});
    })
    .catch(err => res.render(`error`, {error500: 500, error: err}));
});

router.post('/update/:id', extra.checkAuth, function (req, res) {
    Test.getById(req.params.id)
    .then(test => {
        test.title = req.body.title;
        const fileObject = req.files.ava;
        if (typeof fileObject !== `undefined`) {
            return Promise.all([test, extra.uploadToCloudinary(fileObject.data, test)]);
        }
        return Promise.all([test]);
    })
    .then(([test]) => Promise.all([test, Test.update(test)]))
    .then(([test]) => res.redirect(`/tests/` + `${test._id}`))
    .catch(err => res.render(`error`, {error500: 500, error: err}));
});

router.get(`/new`, extra.checkAuth, function (req, res) {
    res.render(`tests/update`, {user: req.user, action: "/tests/new"});
});

router.post('/new', extra.checkAuth, function (req, res) {
    const userId = req.user.id;
    let test = new Test(req.body.title, userId);
    const fileObject = req.files.ava;
    if (typeof fileObject !== `undefined`) {
        Promise.all([test, extra.uploadToCloudinary(fileObject.data, test)])
        .then(() => Test.insert(test))
        .then(test => {
            return Promise.all([test.id, User.addTest(userId, test.id)]);
        })
        .then(([testId]) => res.redirect(`/tests/` + `${testId}`))
        .catch(err => res.render(`error`, {error500: 500, error: err}));
    }
    else {
        Test.insert(test)
        .then(test => {
            return Promise.all([test.id, User.addTest(userId, test.id)]);
        })
        .then(([testId]) => res.redirect(`/tests/` + `${testId}`))
        .catch(err => res.render(`error`, {error500: 500, error: err}));
    }
    
});

router.post('/:id', extra.checkAuth, function (req, res) {
    const testId = req.params.id;
    const userId = req.user.id;
    Test.delete(testId)
    .then(() => User.removeTest(userId, testId))
    .then(() => res.redirect(`/tests`))
    .catch(err => res.render(`error`, {error500: 500, error: err}));
});

router.get(`/:id`, extra.checkAuth, function (req, res) {
    const testId = req.params.id;
    Test.getById(testId)
    .then(test => {
        if (!test) {
            res.render(`error`, {error404: 404});
        }
        else {
            let allQuestions = req.user.questions;
            let testQuestions = test.questions;

            let questionsToAdd = allQuestions.filter(function(item) {
                return !testQuestions.some(function(other) {
                  return item.id === other.id;
                });
              });

            res.render(`test`, {test: test, questionsToAdd: questionsToAdd, user: req.user});
        }
    })
    .catch (err => res.render(`error`, {error500: 500, error: err}));
});

module.exports = router;