const config = require("../config");
const crypto = require('crypto');
const cloudinary = require('cloudinary');
cloudinary.config({
    cloud_name: config.cloud_name,
    api_key: config.api_key,
    api_secret: config.api_secret
});

module.exports = {

    uploadToCloudinary: function(fileBuffer, entity) {
        return new Promise(function(resolve, reject) {
          cloudinary.v2.uploader
            .upload_stream({ resource_type: "image" }, function(error, result) {
              if (error) {
                reject(error);
              } else {
                entity.avaUrl = result.url;
                resolve(result.url);
              }
            })
            .end(fileBuffer);
        });
    },

    sha512: function(password){
        const hash = crypto.createHmac('sha512', config.serverSalt);
        hash.update(password);
        const value = hash.digest('hex');
        return {
            salt: config.serverSalt,
            passwordHash: value
        };
    },

    checkAuth: function(req, res, next) {
        if (!req.user) return res.render(`error`, {error401: 401});
        next();
    },
    
    checkAdmin: function(req, res, next) {
        if (!req.user) res.render(`error`, {error401: 401});
        else if (req.user.role !== 1) res.render(`error`, {error403: 403});
        else next();
    }
};