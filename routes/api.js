const express = require('express');
const passport = require('passport');
const User = require("../models/user");
const Question = require("../models/question");
const Test = require("../models/test");
const extra = require('./extra');
const BasicStrategy = require('passport-http').BasicStrategy;

const router = express.Router();

passport.use(new BasicStrategy(
   function (username, password, done) {
        let hash = extra.sha512(password).passwordHash;
        User.getByUsername(username)
        .then(user => {
            if (user && user.passwordHash === hash) 
                return done(null, user);
            else
             return done(null, false);
        });
    }
));

function checkAuthJson(req, res, next) {
    if (!req.user) res.sendStatus(401).json();
    else next();
}

function checkAdminJson(req, res, next) {
    if (!req.user) res.sendStatus(401).json();
    else if (req.user.role !== 1) res.sendStatus(403).json();
    else next();
}

function auth(req, res, next) {
    if (!req.user) passport.authenticate('basic', {session: false});
    next();
}

router.get(`/`, 
    auth, 
    checkAuthJson,
    function (req, res) {
    res.json();
});

router.get(`/users`,
    auth, 
    checkAdminJson,
    function(req, res) {
        User.getAll()
        .then(users => res.json(users))
        .catch(err => {
           res.status(404).json(err.toString());
       });
    }
);

router.get('/users/me',
    auth,
    checkAuthJson,
    function (req, res) {
       res.json(req.user);
    });

router.get(`/users/:id`,
    auth, 
    checkAdminJson,
    function(req, res) {
        const userId = req.params.id;
        User.getById(userId)
        .then(user =>  {
            if (!user) {
                res.status(404).json(`User with id ${userId} not found`);
            }
            else {
                res.json(user);
            }
        })
        .catch (err => res.status(500).json(err.toString()));
    }
);

router.get(`/users/getByUsername/:username`,
    function(req, res) {
        const username = req.params.username;
        User.getByUsername(username)
        .then(user =>  {
            if (!user) {
                res.json(null);
            }
            else {
                res.json(user.username);
            }
        })
        .catch(err => res.status(500).json(err.toString()));
    }
);

router.post(`/users`,
    auth, 
    checkAuthJson,
    function(req, res) {
        const username = req.body.username;
        User.getByUsername(username)
        .then(user => {
            if (!user) {
                if (req.body.password === req.body.password2) {
                    const fullname = req.body.fullname;
                    const password = req.body.password;
                    return User.insert(new User(username, password, fullname));
                }
                else {
                    res.status(500).send("Passwords do not match").json();
                    return;
                }
            }
            else {
                res.status(500).send("Username already exists").json();
                return;
            }
        })
        .then((user) => res.status(201).json(user))
        .catch(err => res.status(500).json(err.toString()));
    }
);

router.put(`/users`,
    auth, 
    checkAuthJson,
    function(req, res) {
        User.getById(req.user.id)
        .then(user => {
            user.fullname = req.body.fullname;
            user.age = req.body.age;
            user.email = req.body.email;
            const fileObject = req.files.ava;
            if (typeof fileObject !== `undefined`) {
                return Promise.all([user, extra.uploadToCloudinary(fileObject.data, user)]);
            }
            return Promise.all([user]);
        })
        .then(([user]) => User.update(user))
        .then((updatedUser) => res.json(updatedUser))
        .catch(err => res.status(500).json(err.toString()));
    }
);

router.put(`/users/:id`,
    auth, 
    checkAdminJson,
    function(req, res) {
        const userId = req.params.id;
        User.getById(userId)
        .then(user => {
            if (parseInt(user.role) === 0) 
                user.role = 1;
            else
                user.role = 0;
            return User.update(user);
        })
        .then(user => res.json(user))
        .catch(err => res.status(500).json(err.toString()));
    }
);

router.delete(`/users/:id`,
    auth, 
    checkAdminJson,
    function(req, res) {
        const userId = req.params.id;
        User.delete(userId)
        .then((deletion) => res.json(deletion))
        .catch(err => res.status(500).json(err.toString()));
    }
);

router.get(`/questions`,
    auth, 
    checkAuthJson,
    function(req, res) {
        res.json(req.user.questions);
    }
);

router.get(`/questions/:id`,
    auth, 
    checkAuthJson,
    function(req, res) {
        const questId = req.params.id;
        Question.getById(questId)
        .then(question => {
            if (!question) {
                res.status(404).json(`Question with id ${questId} not found`);
            }
            else {
                res.json(question);
            }
        })
        .catch (err => res.status(500).json(err.toString()));
    }
);

router.post(`/questions`,
    auth, 
    checkAuthJson,
    function(req, res) {
        const userId = req.user.id;
        const title = req.body.title;
        const answers = req.body.answers;
        const answersAmount = req.body.count;
        const selectedAnswer = req.body.selectedAnswer;
        let question = new Question(title, answers, answersAmount, selectedAnswer, userId);
        const fileObject = req.files.ava;
        if (typeof fileObject !== `undefined`) {
            Promise.all([question, extra.uploadToCloudinary(fileObject.data, question)])
            .then(([question]) => Question.insert(question))
            .then(question => {
                return Promise.all([question.id, User.addQuestion(userId, question.id)]);
            })
            .then(([questionId]) => res.status(201).json(questionId))
            .catch(err => res.status(500).json(err.toString()));
        }
        else {
            Question.insert(question)
            .then(question => {
                return Promise.all([question.id, User.addQuestion(userId, question.id)]);
            })
            .then(([questionId]) => res.status(201).json(questionId))
            .catch(err => res.status(500).json(err.toString()));
        }
    });

router.put(`/questions/:id`,
    auth, 
    checkAuthJson,
    function(req, res) {
        Question.getById(req.params.id)
        .then(question => {
            question.title = req.body.title;
            question.answers = req.body.answers;
            question.answersAmount = req.body.count;
            question.selectedAnswer = req.body.selectedAnswer;
            const fileObject = req.files.ava;
            if (typeof fileObject !== `undefined`) {
                return Promise.all([question, extra.uploadToCloudinary(fileObject.data, question)]);
            }
            return Promise.all([question]);
        })
        .then(([question]) => Promise.all([question, Question.update(question)]))
        .then(([updatedQuestion]) => res.json(updatedQuestion))
        .catch(err => res.status(500).json(err.toString()));
    }
);

router.delete(`/questions/:id`,
    auth, 
    checkAuthJson,
    function(req, res) {
        const questId = req.params.id;
        const userId = req.user.id;
        Question.delete(questId)
        .then(User.removeQuestion(userId, questId))
        .then((deletion) => res.json(deletion))
        .catch(err => res.status(500).json(err.toString()));
    }
);

router.get(`/tests`,
    auth, 
    checkAuthJson,
    function(req, res) {
        res.json(req.user.tests);
    }
);

router.get(`/testsWall`,
    function(req, res) {
        Test.getActivated()
        .then(tests => res.json(tests))
        .catch (err => res.status(500).json(err.toString()));
    }
);

router.get(`/tests/:id`,
    auth,
    function(req, res) {
        const testId = req.params.id;
        Test.getById(testId)
        .then(test => {
            if (!test) {
                res.status(404).json(`Question with id ${testId} not found`);
            }
            else {
                res.json(test);
            }
        })
        .catch (err => res.status(500).json(err.toString()));
    }
);

router.post(`/tests`,
    auth, 
    checkAuthJson,
    function(req, res) {
        const userId = req.user.id;
        let test = new Test(req.body.title, userId);
        const fileObject = req.files.ava;
        if (typeof fileObject !== `undefined`) {
            Promise.all([test, extra.uploadToCloudinary(fileObject.data, test)])
            .then(() => Test.insert(test))
            .then(test => {
                return Promise.all([test.id, User.addTest(userId, test.id)]);
            })
            .then(([testId]) => res.status(201).json(testId))
            .catch(err => res.status(500).json(err.toString()));
        }
        else {
            Test.insert(test)
            .then(test => {
                return Promise.all([test.id, User.addTest(userId, test.id)]);
            })
            .then(([testId]) => res.status(201).json(testId))
            .catch(err => res.status(500).json(err.toString()));
        }
    }
);

router.put(`/tests/:id`,
    auth, 
    checkAuthJson,
    function(req, res) {
        Test.getById(req.params.id)
        .then(test => {
            test.title = req.body.title;
            const fileObject = req.files.ava;
            if (typeof fileObject !== `undefined`) {
                return Promise.all([test, extra.uploadToCloudinary(fileObject.data, test)]);
            }
            return Promise.all([test]);
        })
        .then(([test]) => Promise.all([test, Test.update(test)]))
        .then(([updatedTest]) => res.json(updatedTest))
        .catch(err => res.status(500).json(err.toString()));
    }
);

router.delete(`/tests/:id`,
    auth, 
    checkAuthJson,
    function(req, res) {
        const testId = req.params.id;
        const userId = req.user.id;
        Test.delete(testId)
        .then(() => User.removeTest(userId, testId))
        .then(() => res.json())
        .catch(err => res.status(500).json(err.toString()));
    }
);

module.exports = router;