const express = require('express');
const Question = require("../models/question");
const User = require("../models/user");
const extra = require("./extra");

const router = express.Router();

router.get(`/`, extra.checkAuth, function (req, res) {
    res.render(`questions`, {user: req.user});
});

router.get(`/new`, extra.checkAuth, function (req, res) {
    res.render(`questions/update`, {user: req.user, action: "/questions/new"});
});

router.get(`/update/:id`, extra.checkAuth, function (req, res) {
    Question.getById(req.params.id)
    .then(question => {
        res.render(`questions/update`, {question: question, action: "/questions/update/" + req.params.id, user: req.user});
    })
    .catch(err => res.render(`error`, {error500: 500, error: err}));
});

router.post('/update/:id', extra.checkAuth, function (req, res) {
    Question.getById(req.params.id)
    .then(question => {
        question.title = req.body.title;
        question.answers = req.body.answers;
        question.answersAmount = req.body.count;
        question.selectedAnswer = req.body.selectedAnswer;
        const fileObject = req.files.ava;
        if (typeof fileObject !== `undefined`) {
            return Promise.all([question, extra.uploadToCloudinary(fileObject.data, question)]);
        }
        return Promise.all([question]);
    })
    .then(([question]) => Promise.all([question, Question.update(question)]))
    .then(([question]) => res.redirect(`/questions/${question._id}`))
    .catch(err => res.render(`error`, {error500: 500, error: err}));
});



router.post('/new', extra.checkAuth, function (req, res) {
    const userId = req.user.id;
    const title = req.body.title;
    const answers = req.body.answers;
    const answersAmount = req.body.count;
    const selectedAnswer = req.body.selectedAnswer;
    let question = new Question(title, answers, answersAmount, selectedAnswer, userId);
    const fileObject = req.files.ava;
    if (typeof fileObject !== `undefined`) {
        Promise.all([question, extra.uploadToCloudinary(fileObject.data, question)])
        .then(([question]) => Question.insert(question))
        .then(question => {
            return Promise.all([question.id, User.addQuestion(userId, question.id)]);
        })
        .then(([questionId]) => {
            res.redirect(`/questions/` + `${questionId}`);
        })
        .catch(err => res.render(`error`, {error500: 500, error: err}));
    }
    else {
        Question.insert(question)
        .then(question => {
            return Promise.all([question.id, User.addQuestion(userId, question.id)]);
        })
        .then(([questionId]) => {
            res.redirect(`/questions/` + `${questionId}`);
        })
        .catch(err => res.render(`error`, {error500: 500, error: err}));
    }
});

router.post('/:id', extra.checkAuth, function (req, res) {
    const questId = req.params.id;
    const userId = req.user.id;
    Question.delete(questId)
    .then(() => User.removeQuestion(userId, questId))
    .then(() => res.redirect(`/questions`))
    .catch(err => res.render(`error`, {error500: 500, error: err}));
});

router.get(`/:id`, extra.checkAuth, function (req, res) {
    const questId = req.params.id;
    Question.getById(questId)
    .then(question => {
        if (!question) {
            res.render(`error`, {error404: 404});
        }
        else {
            res.render(`question`, {question: question, user: req.user});
        }
    })
    .catch (err => res.render(`error`, {error500: 500, error: err}));
});

module.exports = router;