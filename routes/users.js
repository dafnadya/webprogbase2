const express = require('express');
const User = require("../models/user");
const extra = require('./extra');

const router = express.Router();

router.get(`/`, extra.checkAdmin, function (req, res) {
    res.render(`users`, {user: req.user});
});

router.get(`/me`, extra.checkAuth, function (req, res) {
    res.render(`me`, {user: req.user});
});

router.post(`/update/role`, extra.checkAdmin, function (req, res) {
    const userId = req.body.userId;
    User.getById(userId)
    .then(user => {
        if (parseInt(user.role) === 0) 
            user.role = 1;
        else
            user.role = 0;
        return User.update(user);
    })
    .then(user => res.redirect(`/users/${user.id}`))
    .catch(err => res.render(`error`, {error500: 500, error: err}));
});

router.get(`/update`, extra.checkAuth, function (req, res) {
    res.render(`users/update`, {user: req.user});
});

router.post('/update', extra.checkAuth, function (req, res) {
    User.getById(req.user.id)
    .then(user => {
        user.fullname = req.body.fullname;
        user.age = req.body.age;
        user.email = req.body.email;
        user.telegramUsername = req.body.telegramUsername;
        const fileObject = req.files.ava;
        if (typeof fileObject !== `undefined`) {
            return Promise.all([user, extra.uploadToCloudinary(fileObject.data, user)]);
        }
        return Promise.all([user]);
    })
    .then(([user]) => User.update(user))
    .then(() => res.redirect('/users/me'))
    .catch(err => res.render(`error`, {error500: 500, error: err}));
});

router.get(`/:id`, extra.checkAdmin, function (req, res) {
    const userId = req.params.id;
    User.getById(userId)
    .then(user =>  {
        if (!user) {
            res.render(`error`, {error404: 404});
        }
        else {
            res.render(`user`, {tempUser:user, user: req.user});
        }
    })
    .catch (err => res.render(`error`, {error500: 500, error: err}));
});

module.exports = router;