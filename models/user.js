const mongoose = require('mongoose');
const extra = require('../routes/extra');

const UserSchema = new mongoose.Schema({
    username: {type: String, required: true },
    passwordHash: {type: String},
    role: {type: Number, default: 0},
    fullname: {type: String},
    registeredAt: {type: Date, default: Date.now()},
    avaUrl: {type: String, default: "/images/nophoto.png"},
    email: {type: String},
    age: {type: Number},
    isDisabled: {type: Boolean, default: false},
    tests: [{
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Test'
    }],
    questions: [{
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Question'
    }],
    googleId: {type: String},
    telegramUsername: {type: String},
    telegramChatId: {type: Number},
    passedTests: [{
        type: mongoose.Schema.Types.ObjectId,
        ref: 'PassedTest'
    }],
});

const UserModel = mongoose.model('User', UserSchema);

class User {
    constructor(username, password, fullname) {
        this.username = username;
        if (password)
        this.passwordHash = extra.sha512(password).passwordHash;
        this.fullname = fullname;
    }

    static getAll() {
        return UserModel.find()
        .populate('tests')
        .populate('questions');
    }

    static getById(id) {
        return UserModel.findById(id)
        .populate('tests')
        .populate('questions');
    }

    static getByUsername(userName) {
        return UserModel.findOne({username: userName});
    }

    static getByGoogleId(google_id) {
        return UserModel.findOne({googleId: google_id})
        .populate('tests')
        .populate('questions');
    }

    static getByTelegramUsername(telegram_username) {
        return UserModel.findOne({telegramUsername: telegram_username})
        .populate('tests')
        .populate('questions');
    }

    static getUserWithPassedTestsById(id) {
        return UserModel.findById(id)
        .populate({
            path: 'passedTests',
            populate: { path: 'testId' }
        });
    }

    static insert(user) {
        const model = new UserModel(user);
        return model.save();
    }

    static delete(id) {
        return UserModel.findOne({ _id: id })
            .then(() => {
                return Promise.all([
                    UserModel.deleteOne({ _id: id }),
                ]);
            });
    }

    static update(user) {
        return UserModel.findOneAndUpdate({_id: user.id}, user, { new: true });
    }

    static addTest(userId, testId) {
        return UserModel.findOne({ _id: userId })
            .then(user => {
                user.tests.push(testId);
                return UserModel.findOneAndUpdate({ _id: user._id }, user, { new: true });
        });
    }

    static removeTest(userId, testId) {
        return UserModel.findOne({ _id: userId })
            .then(user => {
                user.tests.splice(user.tests.indexOf(testId), 1);
                return UserModel.findOneAndUpdate({ _id: user._id }, user, { new: true });
            });
    }

    static addQuestion(userId, questionId) {
        return UserModel.findOne({ _id: userId })
            .then(user => {
                user.questions.push(questionId);
                return UserModel.findOneAndUpdate({ _id: user._id }, user, { new: true });
        });
    }

    static removeQuestion(userId, questionId) {
        return UserModel.findOne({ _id: userId })
            .then(user => {
                user.questions.splice(user.questions.indexOf(questionId), 1);
                return UserModel.findOneAndUpdate({ _id: user._id }, user, { new: true });
            });
    }

    static addResult(userId, passedTestId) {
        return UserModel.findOne({ _id: userId })
            .then(user => {
                user.passedTests.push(passedTestId);
                return UserModel.findOneAndUpdate({ _id: user._id }, user, { new: true });
        });
    }
}

module.exports = User;