const mongoose = require('mongoose');
const Test = require("./test");

const QuestionSchema = new mongoose.Schema({
    title: {type: String, required: true },
    answers: {
        type: [String],
        required: true
    },
    answersAmount: { type: Number, default: 0 },
    selectedAnswer: { type: Number, default: 0 },
    avaUrl: { type: String, default: "/images/nophoto.png" },
    createdAt: {type: Date, default: Date.now()},
    creator: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'User'
    },
    testsInclusion: [{
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Test'
    }]
});

const QuestionModel = mongoose.model('Question', QuestionSchema);

class Question {
    constructor(title, answers, answersAmount, selectedAnswer, creator, avaUrl, testsInclusion) {
        this.title = title;
        this.answers = answers;
        this.answersAmount = answersAmount;
        this.selectedAnswer = selectedAnswer;
        this.avaUrl = avaUrl;
        this.creator = creator;
        this.testsInclusion = testsInclusion;
    }

    static getAll() {
        return QuestionModel.find()
        .populate('creator')
        .populate('testsInclusion');
    }

    static getById(id) {
        return QuestionModel.findById(id)
        .populate('creator')
        .populate('testsInclusion');
    }
    static insert(question) {
        const model = new QuestionModel(question);
        return model.save();
    }
    static update(question) {
        return QuestionModel.findOneAndUpdate({_id: question.id}, question, { new: true });
    }
    static removedFromTest(questionId, testId) {
        return QuestionModel.findOne({ _id: questionId })
            .then(question => {
                question.testsInclusion.splice(question.testsInclusion.indexOf(testId), 1);
                return QuestionModel.findOneAndUpdate({ _id: question._id }, question, { new: true });
            });
    }
    static addedToTest(questionId, testId) {
        return QuestionModel.findOne({ _id: questionId })
        .then(question => {
            question.testsInclusion.push(testId);
            return QuestionModel.findOneAndUpdate({ _id: question._id }, question, { new: true });
        });
    }
    static delete(id) {
        return QuestionModel.findOne({ _id: id })
            .then(question => {
                for (let testId of question.testsInclusion) {
                    Test.removeQuestion(testId, id);
                }
                return QuestionModel.deleteOne({ _id: id });
            });
    }
}

module.exports = Question;