const mongoose = require('mongoose');

const TestSchema = new mongoose.Schema({
    title: {type: String, required: true },
    questions: [{
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Question'
    }],
    createdAt: {type: Date, default: Date.now()},
    avaUrl: { type: String, default: "/images/nophoto.png" },
    isPassed: {type: Boolean, default: false},
    isActive: {type: Boolean, default: false},
    creator: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'User'
    },
});

const TestModel = mongoose.model('Test', TestSchema);

class Test {
    constructor(title, creator, avaUrl) {
        this.title = title;
        this.avaUrl = avaUrl;
        this.creator = creator;
    }

    static getAll() {
        return TestModel.find()
        .populate('questions')
        .populate('creator');
    }

    static getActivated() {
        return TestModel.find({ isActive: true })
        .populate('questions')
        .populate('creator');
    }

    static getById(id) {
        return TestModel.findById(id)
        .populate('questions')
        .populate('creator');
    }
    static insert(test) {
        const model = new TestModel(test);
        return model.save();
    }
    static update(test) {
        return TestModel.findOneAndUpdate({_id: test.id}, test, { new: true });
    }
    static delete(id) {
        return TestModel.findOne({ _id: id })
            .then( () => {
                return Promise.all([
                    TestModel.deleteOne({ _id: id }),
                ]);
            });
    }
    static addQuestions(testId, questionIds) {
        return TestModel.findOne({ _id: testId })
            .then(test => {
                for (let questionId of questionIds) {
                    test.questions.push(questionId);
                }
                return TestModel.findOneAndUpdate({ _id: test._id }, test, { new: true });
        });
    }
    static removeQuestion(testId, questionId) {
        return TestModel.findOne({ _id: testId })
            .then(test => {
                test.questions.splice(test.questions.indexOf(questionId), 1);
                return TestModel.findOneAndUpdate({ _id: test._id }, test, { new: true });
            });
    }
}

module.exports = Test;