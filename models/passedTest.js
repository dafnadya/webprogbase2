const mongoose = require('mongoose');

const PassedTestSchema = new mongoose.Schema({
    rightAnswers: {type: Number},
    testId: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Test'
    }
});

const PassedTestModel = mongoose.model('PassedTest', PassedTestSchema);

class PassedTest {
    constructor(testId, rightAnswers) {
        this.rightAnswers = rightAnswers;
        this.testId = testId;
    }

    static insert(passedTest) {
        const model = new PassedTestModel(passedTest);
        return model.save();
    }

    // static getAll() {
    //     return UserModel.find()
    //     .populate('tests')
    //     .populate('questions');
    // }

    // static getById(id) {
    //     return UserModel.findById(id)
    //     .populate('tests')
    //     .populate('questions');
    // }
    
    // static delete(id) {
    //     return UserModel.findOne({ _id: id })
    //         .then(() => {
    //             return Promise.all([
    //                 UserModel.deleteOne({ _id: id }),
    //             ]);
    //         });
    // }

    // static update(user) {
    //     return UserModel.findOneAndUpdate({_id: user.id}, user, { new: true });
    // }
}

module.exports = PassedTest;