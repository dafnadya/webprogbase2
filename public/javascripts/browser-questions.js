const data = {
    questions: [],
    _titleFilter: "",
    _page: 1,

    get titleFilter() {
        return this._titleFilter.toLowerCase();
    },

    set titleFilter(value) {
        this._titleFilter = value;
        this._page = 1;
        Ui.setFilter(this.titleFilter);
        Ui.setCurPage(this.page);
        Ui.renderQuestions(this.filteredQuestions);
    },

    get page() {
        return this._page;
    },

    set page(value) {
        this._page = value;
        Ui.setCurPage(this.page);
        Ui.renderQuestions(this.filteredQuestions);
    },

    get filteredQuestions() {
        const itemsPerPage = 5;
        const filterText = this.titleFilter.trim();
        let filteredQuestions = [];
        if (!filterText)
            filteredQuestions = this.questions;
        else
            filteredQuestions = this.questions.filter(x =>
                x.title.toLowerCase().trim()
                .includes(filterText));

        Ui.setTotalQuestions("Total questions: " + filteredQuestions.length);

        let maxPage = parseInt(filteredQuestions.length / itemsPerPage);
        if (filteredQuestions.length % itemsPerPage !== 0 || maxPage === 0) {
            ++maxPage;
        }
        
        Ui.setMaxPage(maxPage);
        const startIndex = (this.page - 1) * itemsPerPage;
        return filteredQuestions.slice(startIndex, startIndex + itemsPerPage);
    },

    setQuestions(questions) {
        Ui.setCurPage(data.page);
        this.questions = questions;
        Ui.renderQuestions(this.filteredQuestions);
    },
};

window.onload = function() {
    Ui.loadListTemplate();
    fetch("/api/v1/questions")
    .then(x => x.json())
    .then(questions => data.setQuestions(questions));
};

window.onhashchange = function() {
    Ui.loadListTemplate();
    fetch("/api/v1/questions")
    .then(x => x.json())
    .then(questions => data.setQuestions(questions));
};

Ui.filterInputEl.addEventListener('input', (e) => {
    data.titleFilter = e.target.value;
});
Ui.clearFilterEl.addEventListener('click', () => {
    data.titleFilter = '';
});
Ui.nextButtonEl.addEventListener('click', () => {
    data.page += 1;
});
Ui.prevButtonEl.addEventListener('click', () => {
    data.page -= 1;
});