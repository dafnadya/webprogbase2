if (parseInt(document.getElementById('roleValue').value) === 1) {
    document.getElementById('role').innerHTML = `Admin`;
    if (document.getElementById('submitButton'))
        document.getElementById('submitButton').innerHTML = "Dismiss";
}
else {
    if (document.getElementById('submitButton'))
        document.getElementById('submitButton').innerHTML = "Promote";
    document.getElementById('role').innerHTML = `User`;
}