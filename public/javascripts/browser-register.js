function checkPassword(input) {
    if (input.value !== document.getElementById('inputPassword').value) {
        input.setCustomValidity('Password must be matching.');
    } else {
        input.setCustomValidity('');
    }
}

function checkUsername(input) {
    fetch('/api/v1/users/getByUsername/' + input.value)
    .then(x => x.json())
    .then(username => {
        if (username) {
            input.setCustomValidity('Username already exists.');
        } else {
            input.setCustomValidity('');
        }
    });
}