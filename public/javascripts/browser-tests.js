const data = {
    tests: [],
    _titleFilter: "",
    _page: 1,

    get titleFilter() {
        return this._titleFilter.toLowerCase();
    },

    set titleFilter(value) {
        this._titleFilter = value;
        this._page = 1;
        Ui.setFilter(this.titleFilter);
        Ui.setCurPage(this.page);
        Ui.renderTests(this.filteredTests);
    },

    get page() {
        return this._page;
    },

    set page(value) {
        this._page = value;
        Ui.setCurPage(this.page);
        Ui.renderTests(this.filteredTests);
    },

    get filteredTests() {
        const itemsPerPage = 5;
        const filterText = this.titleFilter.trim();
        let filteredTests = [];
        if (!filterText)
            filteredTests = this.tests;
        else
            filteredTests = this.tests.filter(x =>
                x.title.toLowerCase().trim()
                .includes(filterText));
        
        Ui.setTotalTests("Total tests: " + filteredTests.length);

        let maxPage = parseInt(filteredTests.length / itemsPerPage);
        if (filteredTests.length % itemsPerPage !== 0 || maxPage === 0) {
            ++maxPage;
        }
        Ui.setMaxPage(maxPage);
        const startIndex = (this.page - 1) * itemsPerPage;
        return filteredTests.slice(startIndex, startIndex + itemsPerPage);
    },

    setTests(tests) {
        Ui.setCurPage(data.page);
        this.tests = tests;
        Ui.renderTests(this.filteredTests);
    },
};

window.onload = function() {
    Ui.loadListTemplate();
    fetch("/api/v1/tests")
    .then(x => x.json())
    .then(tests => data.setTests(tests));
};

window.onhashchange = function() {
    Ui.loadListTemplate();
    fetch("/api/v1/tests")
    .then(x => x.json())
    .then(tests => data.setTests(tests));
};

Ui.filterInputEl.addEventListener('input', (e) => {
    data.titleFilter = e.target.value;
});
Ui.clearFilterEl.addEventListener('click', () => {
    data.titleFilter = '';
});
Ui.nextButtonEl.addEventListener('click', () => {
    data.page += 1;
});
Ui.prevButtonEl.addEventListener('click', () => {
    data.page -= 1;
});