const Ui = {
    filterInputEl: document.getElementById('titleFilter'),
    clearFilterEl: document.getElementById('clearFilter'),
    filteredTestsEl: document.getElementById('filteredTests'),
    curPageEl: document.getElementById('curPage'),
    maxPageEl: document.getElementById('maxPage'),
    nextButtonEl: document.getElementById('btn_next'),
    prevButtonEl: document.getElementById('btn_prev'),

    listTemplate: null,
    async loadListTemplate() {
        const response = await fetch('/templates/testsWall.mst');
        this.listTemplate = await response.text();
    },

    setFilter(filter) { this.filterInputEl.value = filter; },

    setCurPage(page) { 
        this.curPageEl.innerHTML = page.toString();
        this.checkPageButtons();
    },

    setMaxPage(maxpPage) { 
        this.maxPageEl.innerHTML = maxpPage.toString();
        this.checkPageButtons();
    },

    checkPageButtons() {
        if (parseInt(this.curPageEl.innerHTML) === 1)
            this.prevButtonEl.setAttribute("style","visibility:hidden");
        else
            this.prevButtonEl.setAttribute("style","visibility:visible");
        if (parseInt(this.curPageEl.innerHTML) === parseInt(this.maxPageEl.innerHTML))
            this.nextButtonEl.setAttribute("style","visibility:hidden");
        else
            this.nextButtonEl.setAttribute("style","visibility:visible");
    },

    renderTests(tests) {
        const template = this.listTemplate;
        const data = { tests: tests, searchedTitle: this.filterInputEl.value };
        const renderedHTML = Mustache.render(template, data);
        this.filteredTestsEl.innerHTML = renderedHTML;
    },
};