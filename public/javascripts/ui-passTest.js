const Ui = {
    questionEl: document.getElementById('question'),
    curPageEl: document.getElementById('curPage'),
    maxPageEl: document.getElementById('maxPage'),
    nextButtonEl: document.getElementById('btn_next'),
    endButtonEl: document.getElementById('btn_end'),
    testIdEl: document.getElementById('testId'),
    pagesButtonEl: document.getElementById('pages'),

    listTemplate: null,
    async loadListTemplate() {
        const response = await fetch('/templates/passTest.mst');
        this.listTemplate = await response.text();
    },

    setCurPage(page) { 
        this.curPageEl.innerHTML = page.toString();
        this.checkPageButtons();
    },

    setMaxPage(maxpPage) { 
        this.maxPageEl.innerHTML = maxpPage.toString();
        this.checkPageButtons();
    },

    checkPageButtons() {
        if (parseInt(this.curPageEl.innerHTML) === parseInt(this.maxPageEl.innerHTML)) {
            this.nextButtonEl.setAttribute("style","visibility:hidden");
            this.endButtonEl.setAttribute("style","visibility:visible");
        }
        else
            this.nextButtonEl.setAttribute("style","visibility:visible");
    },

    renderTests(question) {
        const template = this.listTemplate;
        let answersArray = [];
        for (let index in question.answers) {
            let obj = {answer: question.answers[index], index: index};
            answersArray.push(obj);
        }
        const data = { question: question, answersArray};
        const renderedHTML = Mustache.render(template, data);
        this.questionEl.innerHTML = renderedHTML;
    },
};