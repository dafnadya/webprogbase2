function loadFile(event) {
    let output = document.getElementById('output');
    output.src = URL.createObjectURL(event.target.files[0]);
};

function AddAnswer() {
    let count = parseInt(document.getElementById("count").value);
    if (count < 5) {
        document.getElementById("count").value = ++count;
        let newNode = document.createElement("span");
        newNode.innerHTML = `<input type="text" name="answers" class="mt-3" required><br>`;
        let button = document.getElementById("add");
        document.getElementById("dynamicAnswers").insertBefore(newNode, button);
        document.getElementById("selectedAnswer").setAttribute("max", count);
    }
    if (count === 5) {
        document.getElementById("add").disabled = true;
    }
}

window.onload = function() {
    let count = parseInt(document.getElementById("count").value);
    if (count === 5) {
        document.getElementById("add").disabled = true;
    }
    document.getElementById("selectedAnswer").setAttribute("max", count);
};