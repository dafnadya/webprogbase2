const data = {
    questions: [],
    _page: 1,
    rightAnswers: 0,

    get page() {
        return this._page;
    },

    set page(value) {
        this._page = value;
        Ui.setCurPage(this.page);
        Ui.renderTests(this.filteredQuestion);
    },

    get filteredQuestion() {
        return this.questions[this.page - 1];
    },

    setQuestions(questions) {
        Ui.setCurPage(data.page);
        this.questions = questions;
        Ui.renderTests(this.filteredQuestion);
        Ui.setMaxPage(parseInt(questions.length));
    },

    checkRightAnswer() {
        let radios = document.getElementsByName('customRadio');
        let checked = -1;

        for (let i = 0, length = radios.length; i < length; i++)
        {
            if (radios[i].checked) {
                checked = i;
                break;
            }
        }
        if (this.filteredQuestion.selectedAnswer === checked + 1)
            this.rightAnswers++;
    }

};

window.onload = function() {
    let testId = Ui.testIdEl.value;
    Ui.loadListTemplate();
    fetch(`/api/v1/tests/${testId}`)
    .then(x => x.json())
    .then(test => {
        data.setQuestions(test.questions);
    });
};

Ui.nextButtonEl.addEventListener('click', () => {
    data.checkRightAnswer();
    data.page += 1;
});

Ui.endButtonEl.addEventListener('click', () => {
    data.checkRightAnswer();
    document.getElementById('rightAnswers').value = data.rightAnswers;
    document.getElementById('questionsAmount').value = data.questions.length;
});