function CreateModal(id) {
    let newNode = document.createElement("span");
    newNode.innerHTML = `<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title text-left">Attention!</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                </div>
                <div class="modal-body">
                    <p>Are you sure?</p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                    <form action="/tests/` + id + `" method="POST">
                        <input type="submit" class="btn btn-danger" value="Confirm">
                    </form>
                </div>
            </div>
        </div>
    </div>`;
    document.getElementById("modalCreate").appendChild(newNode);
}

const Ui = {
    filterInputEl: document.getElementById('titleFilter'),
    clearFilterEl: document.getElementById('clearFilter'),
    filteredTestsEl: document.getElementById('filteredTests'),
    curPageEl: document.getElementById('curPage'),
    maxPageEl: document.getElementById('maxPage'),
    nextButtonEl: document.getElementById('btn_next'),
    prevButtonEl: document.getElementById('btn_prev'),
    deleteButtonEl: document.getElementById('deleteBtn'),
    totalTestsEl: document.getElementById('totalTests'),

    listTemplate: null,
    async loadListTemplate() {
        const response = await fetch('/templates/tests.mst');
        this.listTemplate = await response.text();
    },

    setTotalTests(total) { this.totalTestsEl.innerHTML = total; },

    setFilter(filter) { this.filterInputEl.value = filter; },

    setCurPage(page) { 
        this.curPageEl.innerHTML = page.toString();
        this.checkPageButtons();
    },

    setMaxPage(maxpPage) { 
        this.maxPageEl.innerHTML = maxpPage.toString();
        this.checkPageButtons();
    },

    checkPageButtons() {
        if (parseInt(this.curPageEl.innerHTML) === 1)
            this.prevButtonEl.setAttribute("style","visibility:hidden");
        else
            this.prevButtonEl.setAttribute("style","visibility:visible");
        if (parseInt(this.curPageEl.innerHTML) === parseInt(this.maxPageEl.innerHTML))
            this.nextButtonEl.setAttribute("style","visibility:hidden");
        else
            this.nextButtonEl.setAttribute("style","visibility:visible");
    },

    renderTests(tests) {
        const template = this.listTemplate;
        const data = { tests: tests, searchedTitle: this.filterInputEl.value };
        const renderedHTML = Mustache.render(template, data);
        this.filteredTestsEl.innerHTML = renderedHTML;
    },
};