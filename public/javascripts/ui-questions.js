function CreateModal(id) {
    let newNode = document.createElement("span");
    newNode.innerHTML = `<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title text-left">Attention!</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                </div>
                <div class="modal-body">
                    <p>Are you sure?</p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                    <form action="/questions/` + id + `" method="POST">
                        <input type="submit" class="btn btn-danger" value="Confirm">
                    </form>
                </div>
            </div>
        </div>
    </div>`;
    document.getElementById("modalCreate").appendChild(newNode);
}

const Ui = {
    filterInputEl: document.getElementById('titleFilter'),
    clearFilterEl: document.getElementById('clearFilter'),
    filteredQuestionsEl: document.getElementById('filteredQuestions'),
    curPageEl: document.getElementById('curPage'),
    maxPageEl: document.getElementById('maxPage'),
    nextButtonEl: document.getElementById('btn_next'),
    prevButtonEl: document.getElementById('btn_prev'),
    totalQuestionsEl: document.getElementById('totalQuestions'),
    // get listOfRemoveLinks() {
    //     return document.getElementsByClassName('remove-link');
    // },

    listTemplate: null,
    async loadListTemplate() {
        const response = await fetch('/templates/questions.mst');
        this.listTemplate = await response.text();
    },

    setTotalQuestions(total) { this.totalQuestionsEl.innerHTML = total; },

    setFilter(filter) { this.filterInputEl.value = filter; },

    setCurPage(page) { 
        this.curPageEl.innerHTML = page.toString();
        this.checkPageButtons();
    },

    setMaxPage(maxPage) { 
        this.maxPageEl.innerHTML = maxPage.toString();
        this.checkPageButtons();
    },

    checkPageButtons() {
        if (parseInt(this.curPageEl.innerHTML) === 1)
            this.prevButtonEl.setAttribute("style","visibility:hidden");
        else
            this.prevButtonEl.setAttribute("style","visibility:visible");
        if (parseInt(this.curPageEl.innerHTML) === parseInt(this.maxPageEl.innerHTML))
            this.nextButtonEl.setAttribute("style","visibility:hidden");
        else
            this.nextButtonEl.setAttribute("style","visibility:visible");
    },

    renderQuestions(questions) {
        // remove old list and unsubscribe all handlers
        // for (const link of this.listOfRemoveLinks) {
        //     link.removeEventListener("click", onQuestionRemoveClicked)
        // }
        // update DOM
        const template = this.listTemplate;
        const data = { questions: questions, searchedTitle: this.filterInputEl.value };
        const renderedHTML = Mustache.render(template, data);
        this.filteredQuestionsEl.innerHTML = renderedHTML;
        // subscribe handlers to new links
        // for (const link of this.listOfRemoveLinks) {
        //     link.addEventListener("click", onQuestionRemoveClicked)
        // }
    },
};

// async function onQuestionRemoveClicked(e) {
//     const questionId = parseInt(e.target.getAttribute("question_id"));
//     if (await Question.delete(questionId)) {
//         data.removeQuestion(questionId);
//     }
// }