const config = require("./config");
const User = require("./models/user");

const TelegramBotApi = require('telegram-bot-api');

const telegramBotApi = new TelegramBotApi({
    token: config.bot_token,
    updates: {
        enabled: true  // do message pullreturn
    }
});

telegramBotApi.on('message', onMessage);

function onMessage(message) {
    processRequest(message)
    .catch(err => telegramBotApi.sendMessage({
        chat_id: message.chat.id,
        text: `Something went wrong. Try again later. Error: ${err.toString()}`
    }));
}

function processRequest(message) {
    const tgUsername = message.from.username;
    console.log("username " + tgUsername);
    return User.getByTelegramUsername(tgUsername)
    .then (user => {
        if (message.text === '/start') {
            if (!user) {
                return telegramBotApi.sendMessage({
                    chat_id: message.chat.id,
                    text: `User with Telegram username ${tgUsername} is not registered on http://dafnadya-lab6.herokuapp.com.`
                })
            }
            else {
                if (!user.telegramChatId) {
                    console.log("no id");
                    user.telegramChatId = message.chat.id;
                    User.update(user)
                    .then(user => {
                        return telegramBotApi.sendMessage({
                            chat_id: message.chat.id,
                            text: 'Hello'
                        });
                    });
                }
                else return telegramBotApi.sendMessage({
                    chat_id: message.chat.id,
                    text: 'Hello'
                });
            }
        }
        else {
            return new Promise();
        }
    });
}

module.exports = {
    sendNotificationToAll(text) {
        User.getAll()
        .then(allUsers => {
            for (const user of allUsers) {
                if (user.telegramChatId) {
                    telegramBotApi.sendMessage({
                        chat_id: user.telegramChatId,
                        text: text
                    });
                }
            }
        })
        .catch(err => console.log("ERROR " + err.toString()));
    },
    sendNotificationToUser(text, tgUsername) {
        User.getByTelegramUsername(tgUsername)
        .then(user => {
            if (user.telegramChatId) {
                telegramBotApi.sendMessage({
                    chat_id: user.telegramChatId,
                    text: text
                });
            }
        })
        .catch(err => console.log("ERROR " + err.toString()));
    }
}