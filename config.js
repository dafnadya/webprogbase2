module.exports = {
    ServerPort: process.env.PORT || 3000,
    DatabaseUrl: process.env.DATABASE_URL || 'mongodb://localhost:27017/lab5_db',
    //passwordhash
    serverSalt: process.env.SERVER_SALT,
    //coudinary
    cloud_name: process.env.CLOUD_NAME,
    api_key: process.env.API_KEY,
    api_secret: process.env.API_SECRET,
    //oauth
    clientID: process.env.CLIENT_ID,
    clientSecret: process.env.CLIENT_SECRET,
    // telegram bot
    bot_token: process.env.TELEGRAM_BOT_TOKEN
};