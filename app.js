const express = require('express');
const path = require('path');
const mustache_expr = require('mustache-express');
const bodyParser = require('body-parser');
const busboyBodyParser = require('busboy-body-parser');
const fileUpload = require('express-fileupload');
const mongoose = require('mongoose');
const config = require("./config");
const passport = require('passport');
const cookieParser = require('cookie-parser');
const session = require('express-session');

const app = express();

const port = config.ServerPort;
const dataBaseUrl = config.DatabaseUrl;
const connectOptions = {useNewUrlParser: true};

mongoose.connect(dataBaseUrl, connectOptions)
.then(() => console.log(`Database conected: ${dataBaseUrl}`))
.then(() => app.listen(port, onListen))
.catch(err => console.log(`Error ${err}`));

const viewsDir = path.join(__dirname, 'views');
app.engine('mst', mustache_expr(path.join(viewsDir, 'partials')));
app.set('views', viewsDir);
app.set('view engine', 'mst');
app.use(fileUpload());

function onListen() {
    console.log(`Server running at port ${port}`);
}

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(express.static(path.join(__dirname, 'public')));
app.use(busboyBodyParser({}));
app.use(cookieParser());
app.use(session({
    secret: "Some_secret^string",
    resave: false,
    saveUninitialized: true
}));
app.use(passport.initialize());
app.use(passport.session());

app.use("/users", require("./routes/users"));
app.use("/questions", require("./routes/questions"));
app.use("/tests", require("./routes/tests"));
app.use("/auth", require("./routes/auth"));
app.use("/developer/v1", require("./routes/developer"));
app.use("/api/v1", require("./routes/api"));

app.get(`/`, function (req, res) {
    res.render(`index`, {user: req.user});
});
app.get(`/about`, function (req, res) {
    res.render(`about`, {user: req.user});
});

app.get('*', (req, res) => {
    res.render(`error`, {error404: 404, user: req.user});
});