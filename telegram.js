const config = require("./config");
const User = require("./models/user");

const TelegramBotApi = require('telegram-bot-api');

const telegramBotApi = new TelegramBotApi({
    token: config.bot_token,
    updates: {
        enabled: true  // do message pullreturn
    }
});

telegramBotApi.on('message', onMessage);

function onMessage(message) {
    processRequest(message)
    .catch(err => telegramBotApi.sendMessage({
        chat_id: message.chat.id,
        text: `Something went wrong. Try again later. Error: ${err.toString()}`
    }));
}

function processRequest(message) {
    const tgUsername = message.from.username;
    return User.getByTelegramUsername(tgUsername)
    .then (user => {
        if (message.text === '/start') {
            if (!user) {
                return telegramBotApi.sendMessage({
                    chat_id: message.chat.id,
                    text: `User with Telegram username ${tgUsername} is not registered on http://dafnadya-lab6.herokuapp.com.`
                })
            }
            else {
                if (!user.telegramChatId) {
                    user.telegramChatId = message.chat.id;
                    User.update(user)
                    .then(user => {
                        return telegramBotApi.sendMessage({
                            chat_id: message.chat.id,
                            text: 'Hello'
                        });
                    });
                }
                else return telegramBotApi.sendMessage({
                    chat_id: message.chat.id,
                    text: 'Hello'
                });
            }
        }
        else if (message.text === '/results') {
            if (!user) {
                return telegramBotApi.sendMessage({
                    chat_id: message.chat.id,
                    text: `User with Telegram username ${tgUsername} is not registered on [Quized](http://dafnadya-lab6.herokuapp.com).`,
                    parse_mode: 'Markdown'
                })
            }
            else {
                if (!user.telegramChatId) {
                    telegramBotApi.sendMessage({
                        chat_id: message.chat.id,
                        text: "You aren't subscribed to notification. Choose /start command to subscribe."
                    });
                }
                User.getUserWithPassedTestsById(user.id)
                .then((user) => {
                    let text = "Passed test:\n\n";
                    for (let passedTest of user.passedTests) {
                        let str = "" + `[${passedTest.testId.title}](http://dafnadya-lab6.herokuapp.com/tests/pass/${passedTest.testId.id})` + ": *" + passedTest.rightAnswers + "*/" + passedTest.testId.questions.length + "\n";
                        text += str;
                    }
                    return telegramBotApi.sendMessage({
                        chat_id: message.chat.id,
                        text: text,
                        parse_mode: 'Markdown'
                    });
                })
            }
        }
        else if (message.text.startsWith('/result')) {
            User.getUserWithPassedTestsById(user.id)
                .then((user) => {
                    let text = "";
                    if (message.text.substring(8).trim() === "") {
                        text += "Enter test title."
                    }
                    else for (let passedTest of user.passedTests) {
                        if (passedTest.testId.title.trim() === message.text.substring(8).trim()) {
                            let str = "" + `[${passedTest.testId.title}](http://dafnadya-lab6.herokuapp.com/tests/pass/${passedTest.testId.id})` + ": *" + passedTest.rightAnswers + "*/" + passedTest.testId.questions.length + "\n";
                            text += str;
                        }
                    }
                    if (text === "") {
                        text += "You heven't passed test with such title";
                    }
                    return telegramBotApi.sendMessage({
                        chat_id: message.chat.id,
                        text: text,
                        parse_mode: 'Markdown'
                    });
                })
        }
        else if (message.text === '/help') {
            let text = "*Get started*:\n"
                + " 1) Sign up on [Quized](http://dafnadya-lab6.herokuapp.com)\n"
                + " 2) Enter Telegram username in your profile\n"
                + " 3) Enter /start in this Telegram bot to subscribe notifications\n\n"
                + "/start - subscribe notifications\n"
                + "/results - get results of all tests you've passed\n"
                + "/result \"test title\" - get results of test you've passed by title\n"
                + "/help - get information about commands\n"
                + "/creator - get creator's Telegram contact\n";

            return telegramBotApi.sendMessage({
                chat_id: message.chat.id,
                text: text,
                parse_mode: 'Markdown'
            });
        }
        else if (message.text === '/creator') {
            return telegramBotApi.sendMessage({
                    chat_id: message.chat.id,
                    text: "You can contact me here: @dafnadya",
                    parse_mode: 'Markdown'
            });
        }
        else {
            return telegramBotApi.sendMessage({
                chat_id: message.chat.id,
                text: "I don't know this command. Enter /help to get more information about bot.",
                parse_mode: 'Markdown'
            });
        }
    });
}

module.exports = {
    sendNotificationToAll(text) {
        User.getAll()
        .then(allUsers => {
            for (const user of allUsers) {
                if (user.telegramChatId) {
                    telegramBotApi.sendMessage({
                        chat_id: user.telegramChatId,
                        text: text
                    });
                }
            }
        })
        .catch(err => console.log("ERROR " + err.toString()));
    },
    sendNotificationToUser(text, tgUsername) {
        User.getByTelegramUsername(tgUsername)
        .then(user => {
            if (user.telegramChatId) {
                telegramBotApi.sendMessage({
                    chat_id: user.telegramChatId,
                    text: text,
                    parse_mode: 'Markdown'
                });
            }
        })
        .catch(err => console.log("ERROR " + err.toString()));
    }
}